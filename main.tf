# Inspired by http://label-schema.org/rc1/

variable "name" {
  description = "Human friendly name for the application using the resource."
  default     = "undefined"
}

variable "environment" {
  description = "Environment name such as stage, production, or review."
  default     = "development"
}

variable "description" {
  description = "Text description of the resource. May contain up to 300 characters."
  default     = "undefined"
}

variable "usage" {
  description = "Link to that provides usage instructions"
  default     = "undefined"
}

variable "version" {
  description = "Exact version of resource."
  default     = "undefined"
}

variable "vendor" {
  description = "The organization that produces the Terraform module."
  default     = "undefined"
}

variable "vcs-url" {
  description = "URL for the source code under version control from which the resources are provisioned."
  default     = "undefined"
}

variable "vcs-ref" {
  description = "Identifier for the version of the source code from which the resources are provisioned."
  default     = "undefined"
}

variable "data-sensitivity" {
  description = "Information & risk classification: public, internal, confidential, highly confidential"
  default     = "confidential"
}

output "name" {
  value = "${var.name}"
}

output "environment" {
  value = "${var.environment}"
}

output "description" {
  value = "${var.description}"
}

output "usage" {
  value = "${var.usage}"
}

output "version" {
  value = "${var.version}"
}

output "vendor" {
  value = "${var.vendor}"
}

output "vcs-url" {
  value = "${var.vcs-url}"
}

output "vcs-ref" {
  value = "${var.vcs-ref}"
}

output "data-sensitivity" {
  value = "${var.data-sensitivity}"
}

output "map" {
  value = {
    name             = "${var.name}"
    description      = "${var.description}"
    data-sensitivity = "${var.data-sensitivity}"
    environment      = "${var.environment}"
    usage            = "${var.usage}"
    version          = "${var.version}"
    vendor           = "${var.vendor}"
    vcs-url          = "${var.vcs-url}"
    vcs-ref          = "${var.vcs-ref}"
  }
}
