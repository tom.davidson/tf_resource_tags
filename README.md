# Terraform Resource Tags

Simple module consisting of variables roughly aligning with [label-scema](http://label-schema.org/rc1/) to encourage the use of resource tagging and consistent namespacing with the following:

- name
- description
- data-sensitivity
- environment
- usage
- version
- vendor
- vcs-url
- vcs-ref

## Usage

```terraform
module "resource_tags" {
  source = "git::https://gitlab.com/tom.davidson/tf_resource_tags.git"
}

resource "aws_s3_bucket" "default" {
  bucket = "${var.bucket_name}"
  tags = "${module.resource_tags.map}"
}
```

## Next

- Exclude undefined tags from map and list outputs.
- Read from known files such as package.json, setup.py, etc
